import vtk
from vtk.util import numpy_support
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#%pylab inline

def dicom_import(PathDicom):
    reader = vtk.vtkDICOMImageReader()
    reader.SetDirectoryName(PathDicom)
    reader.Update()
    
    
    # Load dimensions using `GetDataExtent`
    _extent = reader.GetDataExtent()
    ConstPixelDims = [_extent[1]-_extent[0]+1, _extent[3]-_extent[2]+1, _extent[5]-_extent[4]+1]
    
    # Load spacing values
    ConstPixelSpacing = reader.GetPixelSpacing()
    #print('ConstPixelSpacing')
    #print(ConstPixelSpacing)
    #print('x')
    #print(x)
    #print('y')
    #print(y)
    #print('z')
    #print(z)
    # Get the 'vtkImageData' object from the reader
    imageData = reader.GetOutput()
    # Get the 'vtkPointData' object from the 'vtkImageData' object
    pointData = imageData.GetPointData()
    # Ensure that only one array exists within the 'vtkPointData' object
    assert (pointData.GetNumberOfArrays()==1)
    # Get the `vtkArray` (or whatever derived type) which is needed for the `numpy_support.vtk_to_numpy` function
    arrayData = pointData.GetArray(0)
    
    # Convert the `vtkArray` to a NumPy array
    ArrayDicom = numpy_support.vtk_to_numpy(arrayData)
    # Reshape the NumPy array to 3D using 'ConstPixelDims' as a 'shape'
    ArrayDicom = ArrayDicom.reshape(ConstPixelDims, order='F')

    ConstPixelSpacing = np.asarray(ConstPixelSpacing)*0.001 #in [m] 
    return ArrayDicom, ConstPixelSpacing

'''
ArrayDicom, ConstPixelSpacing = dicom_import(PathDicom)
measurement = np.flipud(np.rot90(ArrayDicom[:, 101,:]))

print('measurement.shape')
print(measurement.shape)

"""Plot 3D"""
num_elements = measurement.shape[0] #depending on image
X = np.arange(num_elements)
Y = np.arange(num_elements)

Rx,Ry = np.meshgrid(X,Y)
print('Rx.shape')
print(Rx.shape)
fig4 = plt.figure(4)
diff_fit = measurement
ax = plt.axes(projection='3d')
ax.plot_surface(Rx,Ry,diff_fit,cmap=plt.cm.jet,rcount=num_elements/2, ccount=num_elements/2)#,v    min=0,vmax=0.00001)

plt.show()

plt.figure(1)
plt.axes().set_aspect('equal', 'datalim')
plt.set_cmap(plt.gray())
plt.pcolormesh(x, y, np.flipud(np.rot90(ArrayDicom[:, :, 101])))

plt.show()

plt.figure(2)
plt.axes().set_aspect('equal', 'datalim')
plt.set_cmap(plt.gray())
plt.pcolormesh(x, z, np.flipud(np.rot90(ArrayDicom[:, 101, :])))

plt.show()
'''
