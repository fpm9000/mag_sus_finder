from scipy.optimize import fmin
import matplotlib.pyplot as plt
import numpy as np
import numpy.ma as ma

import setup as ss
import dicom_import_vtk as div
import fit_background as fb
import magnetic_dipole as md
import plot_test_xi as ptx

"""Set script parameters"""
xi_test, sliceNumber, resize, scanner_center_freq, sampleRadius, PathDicom, fig_path = ss.sus_setup()

""" Read measurement data """
ArrayDicom, ConstPixelSpac = div.dicom_import(PathDicom)
measurement_0 = ArrayDicom[:, :, sliceNumber]

"""Unwrap phase""" #Change axis if needed
measurement_0 = np.unwrap(measurement_0, axis=0, period=np.max(measurement_0))
#measurement_0 = np.unwrap(measurement_0, axis=1, period=np.max(measurement_0))

"""Resize image"""
[n_elements_x0, n_elements_y0] = measurement_0.shape
diff_n_elements0 = n_elements_x0 - n_elements_y0
resize_x = resize
resize_y = resize - int(diff_n_elements0/2)
diff_resize = resize_x - resize_y
measurement = measurement_0[0+resize_x:measurement_0.shape[0]-resize_x,0+resize_y:measurement_0.shape[1]-resize_y] #trim of side of image

[n_elements_x, n_elements_y] = measurement.shape
X = np.arange(-n_elements_x*ConstPixelSpac[0]/2, n_elements_x*ConstPixelSpac[0]/2, ConstPixelSpac[0])
Y = np.arange(-n_elements_y*ConstPixelSpac[1]/2, n_elements_y*ConstPixelSpac[1]/2, ConstPixelSpac[1])
Rx,Ry = np.meshgrid(X,Y)

print('max',np.max(measurement))

"""Plot measured field map"""
ptx.plot_2D_xi(X, Y, measurement, fig_path, 'Measured field map')
plt.show()

"""Find position/translation of rod/object"""
z_pos = (np.mean(np.where(measurement == 0)[0])- n_elements_x/2)*ConstPixelSpac[0]
x_pos = (np.mean(np.where(measurement == 0)[1])+0 - n_elements_x/2)*ConstPixelSpac[1]
#x_pos = (np.mean(np.where(measurement == 0)[1]) - n_elements_x/2)*ConstPixelSpac[1]
translation_xz_in_m = np.array([z_pos,x_pos])
print('translation_xz_in_m',translation_xz_in_m)

"""Mask non water signal"""
r = sampleRadius
match_kreis = np.zeros((measurement.shape[0], measurement.shape[1] ))
match_kreis[np.sqrt((Rx-x_pos)**2 + (Ry-z_pos)**2) < r] = 1
ma_measurement = ma.array(measurement, mask= match_kreis, hard_mask=True)

"""Mask non water signal for background fitting"""
r = sampleRadius*3
match_kreis = np.zeros((measurement.shape[0], measurement.shape[1] ))
match_kreis[np.sqrt((Rx-x_pos)**2 + (Ry-z_pos)**2) < r] = 1
ma_fit_measurement = ma.array(measurement, mask= match_kreis, hard_mask=True)

r = sampleRadius

"""Plot 2D to verify Mask """
ptx.plot_2D_xi(X, Y, ma_measurement, fig_path, 'Mask for background fitting')
#ptx.plot_2D_xi(X, Y, ma_fit_measurement, fig_path, 'Mask for background fitting')

""" Find second order polyfit for measurement """
order = 2
Z_fit = fb.polyfit(ma_fit_measurement,Rx,Ry,order)

""" Plot 2D and 3D fitted surface """
#ptx.plot_2D_xi(Rx, Ry, Z_fit, fig_path, '2d fitted surface')
#ptx.plot_3D_surface(Rx, Ry, Z_fit, fig_path, '3d fitted surface')

"""Subtract polyfit to compensate for imperfect shimming"""
clean_ma_measurement = ma_measurement - Z_fit

"""Plot 2D to verify polynomial subtraction"""
#ptx.plot_2D_xi(X,Y,clean_ma_measurement,fig_path,'Measured data after polynomial subtraction')


"""Optimize"""
def match_xi(xi_material, clean_ma_measurement, X,Y, sampleRadius,scanner_center_freq,translation_xy):
    sim_fit = md.ana_sim_magnetic_dipole(xi_material,X,Y,translation_xy, sampleRadius, scanner_center_freq) 
    diff_sim_meas = sim_fit-clean_ma_measurement 
    diff = np.sqrt(np.nansum(np.square(diff_sim_meas)))
    return diff

#plt.show()

theta = 5
xi_H2O = -9.035e-6

init = [xi_test+xi_H2O, theta]
#init = [xi_test, theta]

"""Searching for xi ... """
print('searching for xi ...')
result = fmin(match_xi, init, args=(clean_ma_measurement, X, Y, sampleRadius, scanner_center_freq, translation_xz_in_m), xtol=0.0001, full_output=1)
print('done')


xi_material_Delta_H2O, theta = result[0]
xi_material = -float(xi_material_Delta_H2O)+xi_H2O
xi_material_Delta_H2O = float(xi_material_Delta_H2O)
#xi_material, theta = result[0]

print('xi_material')
print(xi_material)


"""Calculate dipole field with final suscepibility for display """
sim_fit = md.ana_sim_magnetic_dipole(result[0],X,Y,translation_xz_in_m,sampleRadius, scanner_center_freq)
diff_sim_meas_xi = clean_ma_measurement-sim_fit

"""Plot 2D Matched dipole simulation """
#ptx.plot_2D_xi(X,Y,sim_fit,fig_path,'Matched dipole simulation')

"""Plot 2D"""
title = 'Difference of measurement and \n matched dipole simulation'
#ptx.plot_2D_xi(X,Y,diff_sim_meas_xi,fig_path,title)

ptx.plot_3_2D_subplots(X, Y, clean_ma_measurement, sim_fit, diff_sim_meas_xi, fig_path)
plt.show()

