import numpy as np

def polyfit(measurement,Rx_0,Ry_0,order):
    data = measurement.flatten()
    #print('data.shape')
    #print(data.shape)
    X = Rx_0
    #print('X.shape')
    #print(X.shape)
    Y = Ry_0
    XX = X.flatten()
    YY = Y.flatten()
    #print('XX.shape')
    #print(XX.shape)
    XY = np.stack((XX,YY),axis=1)
    #print('XY.shape')
    #print(XY.shape)
    #order = 2    # 1: linear, 2: quadratic
    
    if order == 1:
        # best-fit linear plane
        A = np.c_[XX, YY, np.ones(data.shape[0])]
        C,_,_,_ = np.linalg.lstsq(A, data, rcond=None)    # coefficients
        
        # evaluate it on grid
        Z = C[0]*X + C[1]*Y + C[2]
        
        # or expressed using matrix/vector product
        #Z = np.dot(np.c_[XX, YY, np.ones(XX.shape)], C).reshape(X.shape)
    
    elif order == 2:
        # best-fit quadratic curve
        A = np.c_[np.ones(XY.shape[0]), XY, np.prod(XY, axis=1), XY**2]
        #C,_,_,_ = scipy.linalg.lstsq(A, data)
        C,_,_,_ = np.linalg.lstsq(A, data, rcond=None)    # coefficients
        
        # evaluate it on a grid
        Z = np.dot(np.c_[np.ones(XX.shape), XX, YY, XX*YY, XX**2, YY**2], C).reshape(X.shape)
    return Z
