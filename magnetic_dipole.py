import numpy as np
from skimage.transform import EuclideanTransform
from skimage.transform import rotate
from skimage.transform import matrix_transform

def ana_sim_magnetic_dipole(chi_and_theta,val_x,val_z, translation_xz_in_m, sampleRadius,scanner_center_freq):
    (chi, theta) =chi_and_theta
    #print('chi',chi)

    [tz,tx] = translation_xz_in_m 
    x,z = np.meshgrid(val_x-tx, val_z-tz)

    r = sampleRadius#19     #radius of cylinder (in m)

    chi_out = 0#-9.035e-6 #0 #chi outside cylinder - chi of water =-9.035e-6, but here: =0 b/c we see everything relativ to water;

    ana_Sol = 0.5*(chi*r**2*((z**2-x**2)/(z**2+x**2)**2)+(1-r**2*((z**2-x**2)/(z**2+x**2)**2))*chi_out)*scanner_center_freq;
    ana_Sol = rotate(ana_Sol,theta)

    """set everything inside sample to zero"""
    ana_Sol[np.sqrt(x**2 + z**2) < r] = np.nan

    return ana_Sol
