import matplotlib.pyplot as plt

def plot_2D_xi(X_0, Y_0, measurement, figure_path, figure_title):
    color_map = 'RdBu'
    color_min = -100
    color_max = 100
    plt.rcParams.update({'font.size': 15})

    plt.figure()
    plt.axes().set_aspect('equal')#, 'datalim')
    plt.xlabel('X-axis [mm]')
    plt.tick_params(tickdir='in',bottom=True, top=True, left=True, right=True)
    plt.ylabel('Z-axis [mm]')
    plt.set_cmap(color_map)
    plt.pcolormesh(X_0*1000, Y_0*1000, measurement, vmin=color_min,vmax=color_max)
    plt.title(figure_title)
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Frequency offset [Hz]', rotation=270)
    
    plt.savefig(figure_path + figure_title + ".png", transparent=True)
    
    #plt.show()

def plot_3_2D_subplots(X_0, Y_0, clean_ma_measurement, sim_fit, diff_sim_meas_xi, figure_path):
    color_map = 'RdBu'
    color_min = -100
    color_max = 100
    fig, axs = plt.subplots(nrows=1, ncols=3,sharex=True,sharey=True,figsize=(15,4))
    plt.set_cmap(color_map)

    plt.rcParams.update({'font.size': 12})
    #plt.rcParams.update({'font.size': 15})
    pcm1 = axs[0].pcolormesh(X_0*1000, Y_0*1000, clean_ma_measurement, vmin=color_min,vmax=color_max)
    axs[0].set_aspect('equal', adjustable='box')
    axs[0].tick_params(tickdir='in',bottom=True, top=True, left=True, right=True)
    axs[0].set_title('Measured data \n after polynomial subtraction')
    axs[0].set_ylabel('Z-axis [mm]')
    axs[0].set_xlabel('X-axis [mm]')
    #plt.xlabel('x-label')#axs[0].ax.set(xlabel='X-axis [mm]',ylabel='Z-axis [mm]')

    pcm2 = axs[1].pcolormesh(X_0*1000, Y_0*1000, sim_fit, vmin=color_min,vmax=color_max)
    axs[1].set_aspect('equal', adjustable='box')
    axs[1].tick_params(tickdir='in',bottom=True, top=True, left=True, right=True)
    axs[1].set_title('Matched dipole simulation \n')
    axs[1].set_xlabel('X-axis [mm]')
    #axs[1].ax.set(xlabel='X-axis [mm]')

    pcm3 = axs[2].pcolormesh(X_0*1000, Y_0*1000, diff_sim_meas_xi, vmin=color_min,vmax=color_max)
    axs[2].set_aspect('equal', adjustable='box')
    axs[2].tick_params(tickdir='in',bottom=True, top=True, left=True, right=True)
    axs[2].set_title('Difference of measurement and \n matched dipole simulation')
    axs[2].set_xlabel('X-axis [mm]')
    #axs[2].ax.set(xlabel='X-axis [mm]')

    cbar = fig.colorbar(pcm3,ax=axs[:])
    cbar.ax.set_ylabel('Frequency offset [Hz]', rotation=270)

    plt.savefig(figure_path + "figure9.png",dpi=1200,transparent=True)

""" 3D plot fitted surface """
def plot_3D_surface(Rx, Ry, Z_fit, figure_path, figure_title):
    fig = plt.figure(4)
    ax = fig.add_subplot(projection='3d')
    #ax = fig.gca(projection='3d')
    ax.plot_surface(Rx*1000, Ry*1000, Z_fit, rstride=1, cstride=1, alpha=0.5)
    plt.xlabel('X-axis [mm]')
    plt.ylabel('Z-axis [mm]')
    ax.set_zlabel('Frequency offset [Hz]')
    ax.axis('tight')
    plt.title('2. order polynomial surface fit')
    
    plt.savefig(figure_path + figure_title + "png", transparent=True)
