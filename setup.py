
def sus_setup():
    xi_test = -9.6e-6
    
    sliceNumber = 29 #24 #24,23,22,35 #enno2

    resize =110
    
    scanner_center_freq = 400331083.6 #Wikipedia: 42576000 #Hz/T Messungen Herbst 2019
    
    sampleRadius = 0.003
    sampleRadius = 0.00044
    sampleRadius = 0.00042
    #sampleRadius = 0.00040

    """Path to Figures"""
    fig_path = '/home/wehkamp/git/mag_sus_finder/figures_enno/'
    
    """Path to Data"""
    #PathDicom ='/home/wehkamp/myDataDir/mag_sus_data/151_2023_1018_susc_151_2023_1018_susc_1_E17_P1/'
    #PathDicom ='/home/wehkamp/myDataDir/mag_sus_data/151_2023_1018_susc_151_2023_1018_susc_1_E11_P1/'
    #Freshly Pulled
    #PathDicom ='/home/wehkamp/myDataDir/mag_sus_data/151_2023_1031_susc_151_2023_1031_susc_1_E10_P1/'
    #Annealed
    PathDicom ='/media/wehkamp/data_store/myDataDir/mag_sus_data/151_2023_1031_susc_151_2023_1031_susc_1_E34_P1/'

    #PathDicom = '/home/wehkamp/myDataDir/mag_sus_data/151_2023_0929_highresfieldmap_151_2023_0929_highresfieldmap_1_E21_P1/'

    #resize =60
    #PathDicom = '/home/wehkamp/myDataDir/mag_sus_data/james/101_2019_0118_UVGlueFieldMap_101_2019_0118_UVGlueFieldMap_6__E12_P1/'
    #PathDicom = '/home/wehkamp/myDataDir/mag_sus_data/james/101_2019_0118_UVGlueFieldMap_101_2019_0118_UVGlueFieldMap_6__E15_P1/'
    #PathDicom = '/home/wehkamp/myDataDir/mag_sus_data/james/101_2019_0118_UVGlueFieldMap_101_2019_0118_UVGlueFieldMap_6__E20_P1/'
    #PathDicom = '/home/wehkamp/myDataDir/mag_sus_data/james/101_2019_0118_UVGlueFieldMap_101_2019_0118_UVGlueFieldMap_6__E25_P1/'
    #PathDicom ='/home/wehkamp/git/mag_sus_finder/example_data/'    

    return(xi_test, sliceNumber, resize, scanner_center_freq, sampleRadius, PathDicom, fig_path)
